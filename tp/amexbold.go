package main

import (
	"encoding/json"
	bolt "github.com/coreos/bbolt"
	"log"
	"strconv"
)

type Cliente struct {
	Nrocliente                  int
	Nombre, Apellido, Domicilio string
	teléfono                    int
}
type Tarjeta struct {
	Nrotarjeta, Nrocliente, Validadesde int
	Validahasta, Codseguridad           int
	Limitecompra                        float64
	Estado                              string
}
type Comercio struct {
	Nrocomercio            int
	Nombre, Domicilio      string
	Codigopostal, Telefono int
}
type Consumo struct {
	Nrotarjeta, Codseguridad, Nrocomercio int
	Monto                                 float64
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {
	db, err := bolt.Open("amex.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	cli1 := Cliente{1, "Diego", "Rodriguez", "San Martin 545", 11153456723}
	data0, err := json.Marshal(cli1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cli1.Nrocliente)), data0)

	cli2 := Cliente{2, "Claudia", "Ruiz", "Roma 234", 11153456094}
	data1, err := json.Marshal(cli2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cli2.Nrocliente)), data1)

	cli3 := Cliente{3, "Romina", "Cabrera", "Aguero 228", 11123456124}
	data2, err := json.Marshal(cli3)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cli3.Nrocliente)), data2)

	com1 := Comercio{1, "Supermercado Argenchino", "Av. Maipu 748", 17445401, 11657893411}
	data3, err := json.Marshal(com1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(com1.Nrocomercio)), data3)

	com2 := Comercio{2, "Supermercado Amanecer", "Av. Moreno 878", 16445741, 11689853418}
	data4, err := json.Marshal(com2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(com2.Nrocomercio)), data4)

	com3 := Comercio{3, "Almacen Don Gabriel", "Av. Mosconi 6542", 17364245, 11667886989}
	data5, err := json.Marshal(com3)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "comercio", []byte(strconv.Itoa(com3.Nrocomercio)), data5)

	t1 := Tarjeta{4338645185721981, 1, 201706, 202502, 6589, 40000.65, "vigente"}
	data6, err := json.Marshal(t1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(t1.Nrotarjeta)), data6)

	t2 := Tarjeta{4338645185721981, 2, 201706, 202502, 6589, 40000.65, "vigente"}
	data7, err := json.Marshal(t2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(t1.Nrotarjeta)), data7)

	t3 := Tarjeta{4770644723516981, 3, 201904, 202512, 7589, 44110.85, "anulada"}
	data8, err := json.Marshal(t3)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(t3.Nrotarjeta)), data8)

	con1 := Consumo{4770645187514765, 3678, 1, 2504.4}
	data9, err := json.Marshal(con1)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "consumo", []byte(strconv.Itoa(con1.Nrotarjeta)), data9)

	con2 := Consumo{4770645187515871, 6389, 3, 1902}
	data10, err := json.Marshal(con2)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "consumo", []byte(strconv.Itoa(con2.Nrotarjeta)), data10)

	con3 := Consumo{258645187514751, 9685, 4, 5500}
	data11, err := json.Marshal(con3)
	if err != nil {
		log.Fatal(err)
	}
	CreateUpdate(db, "consumo", []byte(strconv.Itoa(con3.Nrotarjeta)), data11)

}
